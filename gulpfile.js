import gulp from 'gulp';
import browserSync from 'browser-sync';
import autoprefixer from 'gulp-autoprefixer';
import clean from 'gulp-clean';
import cleanCss from 'gulp-clean-css';
import concat from 'gulp-concat';
import imagemin from 'gulp-imagemin';
import minifyJs from 'gulp-js-minify';
import fileinclude from 'gulp-file-include'
import minifyHtml from 'gulp-htmlmin';
import dartSass from 'sass';
import gulpSass from 'gulp-sass';

const sass = gulpSass(dartSass);
const { src, dest, watch, series, parallel } = gulp;

const cleanDist = function() {
    return src("./dist/**/*", {read: false})
        .pipe(clean());
};

const htmlTask = function() {
    return src('./src/index.html')
        .pipe(fileinclude())
        .pipe(minifyHtml({ collapseWhitespace: true }))
        .pipe(dest('./dist/'))
        .pipe(browserSync.stream());
};

const stylesTask = function() {
    return src('./src/scss/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(cleanCss({compatibility: 'ie8'}))
        .pipe(autoprefixer({
                cascade: false,
                grid: true,
            })
        )
        .pipe(concat('style.min.css'))
        .pipe(dest('./dist/css/'))
        .pipe(browserSync.stream())
};

const scriptsTask = function () {
    return src('./src/js/**/*.js')
        .pipe(concat('scripts.min.js'))
        .pipe(minifyJs())
        .pipe(dest('./dist/js/'))
        .pipe(browserSync.stream())
};

const imagesTask = function() {
    return src('./src/img/**/*.+(png|jpeg|jpg|gif|svg)')
        .pipe(imagemin())
        .pipe(dest('./dist/img'))
};

const server = function() {
    browserSync.init({server: {baseDir: `./dist`,},
        notify: false,
        port: 3000,
    });
};

const watching = function() {
    watch("./src/**/*.scss", stylesTask);
    watch("./src/**/*.html", htmlTask);
    watch("./src/**/*.js", scriptsTask);
    watch("./src/img/**/*.+(png|jpg|jpeg|gif|svg)", imagesTask);
};

const dev = series(imagesTask, stylesTask, scriptsTask, htmlTask, parallel(watching, server));

const build = series(cleanDist, imagesTask, stylesTask, scriptsTask, htmlTask);

export { dev, build };

// exports.dev = dev
// exports.build = build

// devTask

// очищення папки dist;
// компіляція scss файлів у css;
// Додавання вендорних префіксів до CSS властивостей для підтримки останніх кількох версій кожного з браузерів;
// видалення невикористовуваного CSS-коду;
// конкатенація всіх js файлів в один scripts.min.js та його мініфікація;
// копіювання мініфікованих підсумкових styles.min.css та scripts.min.js файлів у папку dist;
// оптимізація картинок та копіювання їх у папку dist/img;

// buildTask

// апуск сервера та подальше відстеження змін *.js та *.scss файлів у папці src;
// При зміні - перезбірка та копіювання об'єднаних та мініфікованих файлів styles.min.css та scripts.min.js в папку dist, перезавантаження вашої html-сторінки.
