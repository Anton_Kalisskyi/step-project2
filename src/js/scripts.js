const burgerBtn = document.getElementById('burger-mobile');
burgerBtn.addEventListener('click', () => {
    document.getElementById('menu-mobile').classList.toggle('mobile-active');
    burgerBtn.classList.toggle('mobile-active');
})